Par défaut le Repository récupère le nom de la table à partir du nom de l'entité ex : `SchoolEmployee => school_employee`. Sinon si le nom de la table est différent du nom de l'entité, il peut être renseigné dans l'entité en ajoutant une propriété `public string $table`.

orm documentation => https://wired-orm.readthedocs.io/en/stable/getting-started/

fomantic ui installation => https://fomantic-ui.com/introduction/getting-started.html