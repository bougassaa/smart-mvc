<?php


namespace App;


use HaydenPierce\ClassFinder\ClassFinder;
use Pecee\SimpleRouter\SimpleRouter;
use ReflectionClass;
use Tracy\Debugger;
use zpt\anno\Annotations;

class Router extends SimpleRouter
{
    public function process()
    {
        try {
            $this->parseRoutes();
            self::start();
        } catch (\Exception $e) {
            if (IS_DEV_ENV) {
                Debugger::exceptionHandler($e);
            } else {
                echo Template::create()->render('error.html.twig', [
                    'exception' => $e
                ]);
            }
        }
    }

    /**
     * Parse routes from controllers
     */
    private function parseRoutes()
    {
        $classes = ClassFinder::getClassesInNamespace('App\Controller', ClassFinder::RECURSIVE_MODE);

        // parse all controller classes
        foreach ($classes as $class) {
            $reflection = new ReflectionClass($class);
            // parse all methods of class
            foreach ($reflection->getMethods() as $methodReflector) {
                $annot = new Annotations($methodReflector);

                // check if method has Route annotation
                if ($annot->hasAnnotation('route')) {
                    $route = $annot->offsetGet('route');

                    if (!empty($route)) {
                        $callback = [$class, $methodReflector->getName()];
                        // default method => GET
                        $method = [Request::REQUEST_TYPE_GET];
                        $name = null;
                        $url = trim_url($_ENV['ROOT_URL']);;

                        if (is_string($route)) {
                            $url .= $route;
                        } elseif (is_array($route)) {
                            if (isset($route['method'])) {
                                $method = is_array($route['method']) ? $route['method'] : [$route['method']];
                            }

                            $url .= $route['url'] ?? '/';
                            $name = $route['name'] ?? null;
                        }

                        $routeUrl = self::match($method, $url, $callback);

                        if ($name) {
                            $routeUrl->name($name);
                        }
                    }
                }
            }
        }
    }
}