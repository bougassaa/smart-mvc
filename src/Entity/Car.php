<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="car")
 */
class Car
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length="100")
     */
    private string $brand;

    /**
     * @ORM\Column(type="string", length="100")
     */
    private string $model;

    /**
     * @ORM\Column(type="integer")
     */
    private int $hp;

    /**
     * @ORM\ManyToOne(targetEntity="Owner", inversedBy="cars")
     */
    private ?Owner $owner;

    public function getOwner(): ?Owner
    {
        return $this->owner;
    }

    public function setOwner(Owner $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @ORM\Column(type="string", length="100")
     */
    private string $serialNumber;

    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    public function setSerialNumber($value)
    {
        $this->serialNumber = $value;

        return $this;
    }

    public function getHp()
    {
        return $this->hp;
    }

    public function setHp($value)
    {
        $this->hp = $value;

        return $this;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($value)
    {
        $this->model = $value;

        return $this;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($value)
    {
        $this->brand = $value;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }
}