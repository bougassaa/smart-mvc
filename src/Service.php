<?php


namespace App;


abstract class Service
{
    protected static array $services = [];

    /**
     * @param string $name
     * @param mixed|object $service
     */
    public static function add(string $name, $service)
    {
        self::$services[$name] = $service;
    }

    /**
     * @param string $name
     * @return mixed|object
     * @throws \Exception
     */
    public static function get(string $name)
    {
        if (isset(self::$services[$name])) {
            return self::$services[$name];
        }

        throw new \Exception(sprintf("Service [%s] not found !", $name));
    }
}