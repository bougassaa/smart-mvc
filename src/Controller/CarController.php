<?php


namespace App\Controller;


use App\Entity\Car;
use App\Form\CarForm;
use Nette\Forms\Form;

class CarController extends Controller
{
    /**
     * @Route(url="/car/index", method="get", name="car_index")
     */
    public function index()
    {
        $repo = $this->getManager()->getRepository(Car::class);

        return $this->twig->render('car/index.html.twig', [
            'cars' => $repo->findAll()
        ]);
    }

    /**
     * @Route(url="/car/add", name="car_add", method=["get", "post"])
     */
    public function add()
    {
        $form = new CarForm();

        if ($form->isSuccess()) {
            $data = $form->getValues();

            $car = (new Car())
                ->setBrand($data['brand'])
                ->setModel($data['model'])
                ->setHp($data['hp'])
                ->setSerialNumber($data['serialNumber']);

            $manager = $this->getManager();
            $manager->persist($car);
            $manager->flush();

            return $this->redirectTo("car_show", ['id' => $car->getId()]);
        }

        return $this->twig->render('car/add.html.twig', [
            'form' => $form
        ]);
    }

    /**
     * @Route(url="/car/delete/{id}", name="car_delete")
     */
    public function delete($id)
    {
        $manager = $this->getManager();
        $repo = $manager->getRepository(Car::class);
        $car = $repo->find($id);
        $manager->remove($car);
        $manager->flush();

        return $this->redirectTo('car_index');
    }

    /**
     * @Route(url="/car/{id}", name="car_show")
     */
    public function show($id)
    {
        $repo = $this->getManager()->getRepository(Car::class);
        $car = $repo->find($id);

        if (!$car) {
            throw new \Exception("Car not found");
        }

        return $this->twig->render('car/show.html.twig', [
            'car' => $car
        ]);
    }
}