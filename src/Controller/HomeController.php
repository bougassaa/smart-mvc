<?php


namespace App\Controller;


class HomeController extends Controller
{
    /**
     * @Route(url="/",name="home")
     */
    public function index()
    {
        return $this->twig->render('home/index.html.twig');
    }
}