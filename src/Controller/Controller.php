<?php


namespace App\Controller;


use App\Request;
use App\Service;
use App\Template;
use Doctrine\ORM\EntityManager;
use Twig\Environment;

abstract class Controller
{
    protected Request $request;
    protected Environment $twig;

    public function __construct()
    {
        $this->request = new Request();
        $this->twig = Template::create();
    }

    protected function redirectTo($route, ?array $parameters = null, ?int $code = null)
    {
        redirect(\url($route, $parameters), $code);

        return true;
    }

    protected function getManager(): EntityManager
    {
        return Service::get('manager');
    }
}