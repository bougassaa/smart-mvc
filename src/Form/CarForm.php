<?php


namespace App\Form;


use Nette\Forms\Form;

class CarForm extends Form
{
    public function __construct(string $name = null)
    {
        parent::__construct($name);

        $this->addText('brand', 'Marque')
            ->setDefaultValue('Volkswagen')
            ->setRequired(true);

        $this->addText('model', 'Modèle')
            ->setRequired(true);

        $this->addInteger('hp', 'Puissance')
            ->setRequired(true)
            ->addRule(self::MIN, null, 20);

        $this->addText('serialNumber', 'Numéro de série');
        $this->addSubmit('submit', 'Ajouter');
    }
}