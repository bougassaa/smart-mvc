<?php


namespace App;


class Request extends \Pecee\Http\Request
{
    private array $request;

    public function __construct()
    {
        parent::__construct();
        $this->request = $_REQUEST;
    }

    public function get($key)
    {
        return $this->request[$key] ?? null;
    }

    public function has($key)
    {
        return isset($this->request[$key]);
    }

    public function isEmpty($key)
    {
        return !isset($this->request[$key]) || empty($this->request[$key]);
    }

    public function getAll()
    {
        return $this->request;
    }

    public function isPostMethod(): bool
    {
        return $this->isPostBack();
    }
}