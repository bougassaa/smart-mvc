<?php


namespace App;


use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

class Template
{
    public static function create()
    {
        $twig = new Environment(
            new FilesystemLoader(DIR_TEMPLATES)
        );

        self::addFunctions($twig);

        return $twig;
    }

    private static function addFunctions(Environment &$twig)
    {
        $twig->addFunction(
            new TwigFunction('asset', function ($asset) {
                $root = trim_url($_ENV['ROOT_URL']);
                return "/$root/public/$asset";
            })
        );

        $twig->addFunction(
            new TwigFunction('url', 'url')
        );
    }
}