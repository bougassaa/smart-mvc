DROP DATABASE IF EXISTS `smart-mvc`;
CREATE DATABASE `smart-mvc`;

USE `smart-mvc`;

CREATE TABLE car (
     id INT auto_increment,
     serial_number varchar(100) NULL,
     brand varchar(100) NULL,
     model varchar(100) NULL,
     hp INT NULL,

     PRIMARY KEY (id)
);

CREATE TABLE owner (
     id INT auto_increment,
     first_name varchar(100) NULL,
     last_name varchar(100) NULL,

     PRIMARY KEY (id)
);

CREATE TABLE car_owner (
    car_id INT NOT NULL,
    owner_id INT NOT NULL,

    FOREIGN KEY (car_id) REFERENCES car(id),
    FOREIGN KEY (owner_id) REFERENCES owner(id)
);