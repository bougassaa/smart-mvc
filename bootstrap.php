<?php

require 'vendor/autoload.php';
require 'helpers.php';

use App\Service;
use App\Template;
use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Dotenv\Dotenv;
use Tracy\Debugger;

const APP_DEV = 'dev';
const APP_PROD = 'prod';
const APP_TEST = 'test';

const DIR_ENTITIES = __DIR__ . '/src/Entity';
const DIR_CONTROLLERS = __DIR__ . '/src/Controller';
const DIR_TEMPLATES = __DIR__ . '/templates';

// extract variable from .env
$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

define("IS_DEV_ENV", $_ENV['APP_ENV'] == APP_DEV);

// debugger configuration
Debugger::enable(!IS_DEV_ENV);
Debugger::$showBar = $_ENV['SHOW_DEBUGG_BAR'] == 'true';

try {
    // create database connection
    $conn = DriverManager::getConnection([
        'dbname'    => $_ENV['DB_NAME'],
        'user'      => $_ENV['DB_USER'],
        'password'  => $_ENV['DB_PASSWORD'],
        'host'      => $_ENV['DB_HOST'],
        'driver'    => $_ENV['DB_DRIVER']
    ]);

    $config = Setup::createAnnotationMetadataConfiguration(
        [DIR_ENTITIES],
        true, // turn it to false, make Connection refused error => linked to the cache ?
        null,
        null,
        false
    );
} catch (Exception $e) {
    echo Template::create()->render('error.html.twig', [
        'exception' => $e
    ]);
    exit;
}

Service::add('manager', EntityManager::create($conn, $config));