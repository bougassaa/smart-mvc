<?php

require_once "bootstrap.php";

use App\Service;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

return ConsoleRunner::createHelperSet(
    Service::get('manager')
);