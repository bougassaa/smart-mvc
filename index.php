<?php

require 'bootstrap.php';

use App\Router;

$router = new Router();
$router->process();